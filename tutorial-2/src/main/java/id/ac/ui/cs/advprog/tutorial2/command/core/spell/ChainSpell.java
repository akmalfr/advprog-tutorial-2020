package id.ac.ui.cs.advprog.tutorial2.command.core.spell;
import java.util.ArrayList;

public class ChainSpell implements Spell {
    private ArrayList<Spell> spells;

    public ChainSpell(ArrayList<Spell> spells) {
        this.spells = spells;
    }

    public void cast() {
        for(int i=0; i<spells.size(); i++){
            spells.get(i).cast();
        }
    }

    public void undo() {
        for(int i=spells.size() - 1; i>=0; i--) {
            spells.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
