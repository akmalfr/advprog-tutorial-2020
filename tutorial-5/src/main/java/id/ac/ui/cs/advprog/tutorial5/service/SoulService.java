package id.ac.ui.cs.advprog.tutorial5.service;

// TODO: Import Soul.java, Optional, dan List
import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface SoulService {
    public List<Soul> findAll();
    public Optional<Soul> findSoul(Long id);
    public void erase(Long id); //delete
    public Soul rewrite(Soul soul); //update
    public Soul register(Soul soul); //create
}
